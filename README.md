# sam-dynamodb-local
A hands-on tutorial on using SAM with DynamoDB Local.

## Develop and test serverless applications locally with DynamoDB local.


#### Steps
1. Create docker network
    `docker network create lambda-local`
  
2. Start DynamoDB Local by executing the following at the command prompt:  
	`docker run --network=lambda-local --name dynamo -p 8000:8000 amazon/dynamodb-local`  
    This will run the DynamoDB local in a docker container at port 8000.  

    later, 
    `docker container ls -a`
    Note container ***ID***
    `docker run ID`

3. At the command prompt, list the tables on DynamoDB Local by executing:  
    `aws dynamodb list-tables --endpoint-url http://localhost:8000`  

4. An output such as the one shown below confirms that the DynamoDB local instance has been installed and running:  
```
    {  
      "TableNames": []   
    }    
```

5. At the command prompt, create the SimpleSocialTable table by executing:  
    `aws dynamodb create-table --cli-input-json file://json/create-social-table.json --endpoint-url http://localhost:8000`  
      
      **Note:** If you misconfigured your table and need to delete it, you may do so by executing the following command:  
        `aws dynamodb delete-table --table-name SimpleSocial --endpoint-url http://localhost:8000`  

6. At the command prompt, start the local API Gateway instance by executing:  
    `sam local start-api --docker-network lambda-local --env-vars json/env.json`  