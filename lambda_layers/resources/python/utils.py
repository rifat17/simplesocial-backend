from pprint import pp as pretyprint

def log_in_console(msg, data):
    print(f'\n{msg}\n')
    pretyprint(data)
    print('\n')