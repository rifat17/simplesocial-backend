import pydantic
from typing import List, Optional
from datetime import datetime


class AddCommentReqBody(pydantic.BaseModel):
    comment: str
    commentedBy: str
    postID: str
    PK: Optional[str]
    SK: Optional[str]
    GSI1PK: Optional[str]
    GSI1SK: Optional[str]
    commentedAt: str = None


class Comments(pydantic.BaseModel):
    comments: List[AddCommentReqBody]

class GetCommentsByPostIDReqBody(pydantic.BaseModel):
    postID: str




# Comment,
# commentedby=USER#<USERNAME>, 
# postID=POST#<KSUID>, 
# commented_at
# PK=POST#<KSUID>,
# SK=#COMMENT#<KSUID>,
# GSI1PK=POST#<KSUID>,
# GSI1SK=#COMMENT#<KSUID>,