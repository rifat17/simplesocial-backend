import pydantic
from typing import List, Optional
from datetime import datetime


class AddPostReqBody(pydantic.BaseModel):
    postOwner: str
    postContent: str
    postType: str
    showMetadata: bool
    posted_at : str = None
    PK: Optional[str]
    SK: Optional[str]
    GSI1SK: Optional[str]
    GSI1PK: str = 'ALL_POST'

class Posts(pydantic.BaseModel):
    posts: List[AddPostReqBody]

class GetPostByPostIDReqBody(pydantic.BaseModel):
    postID: str

class GetPostByUsername(pydantic.BaseModel):
    username: str
    postID: Optional[str] = None