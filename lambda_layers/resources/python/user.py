import pydantic
from typing import Optional


class UserBody(pydantic.BaseModel):
    username: str
    age: int
    address : str
    password: str
    PK: Optional[str]
    SK: Optional[str]
    GSI1PK: Optional[str]
    GSI1SK: Optional[str]

class LoginBody(pydantic.BaseModel):
    username: str
    password: str