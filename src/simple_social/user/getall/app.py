import os
import json
import boto3
from boto3.dynamodb.conditions import Key
from botocore.exceptions import ClientError

def lambda_handler(event, context):

    # Get environment variables

    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    aws_environment = os.environ['AWSENV']
    dev_environment = os.environ['DEVENV']

    client = boto3.client('dynamodb', endpoint_url="http://dynamo:8000")

    print(table_name, region, aws_environment)
    headers = {
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin': '*'
            }

    # Check if executing locally or on AWS, and configure DynamoDB connection accordingly.
    if aws_environment == "AWS_SAM_LOCAL":
        # SAM LOCAL
        # Environment is Linux
        social_table = boto3.resource('dynamodb', endpoint_url="http://dynamo:8000").Table(table_name)
    else:
        # AWS
        social_table = boto3.resource('dynamodb', region_name=region).Table(table_name)


    # Print statement for debugging.
    # print(event)

    # Load body JSON for processing
    try:
        print(event['body'])
        bodydict = json.loads(event['body'])
    except:
        return {
            'statusCode': 400,
            'body': 'malformed JSON',
            'headers' : headers
        }

    # GET Method

    print("In GETALL method")

    try:
        print(bodydict)
        # response = social_table.scan()
        # response = social_table.query(
        #     ScanIndexForward=False,
        #     KeyConditionExpression = Key('PK').eq(f'USER#{bodydict["PK"]}') and Key('SK').eq(f'USER#{bodydict["SK"]}')

        # )
        response = client.query(
            TableName=table_name,
            KeyConditionExpression='#pk = :pk AND #sk = :sk',
            ExpressionAttributeNames={
                '#pk' : 'PK',
                '#sk' : 'SK'
            },
            ExpressionAttributeValues = {
                ':pk' : { 'S' : f'USER#{bodydict["PK"]}' },
                ':sk' : { 'S' : f'USER#{bodydict["SK"]}' },
            }
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
        return {'statusCode': 400, 'body': e.response['Error']['Message'], 'headers': headers}
    else:
        # item = response['Item']
        print(response)
        print("GetItem succeeded:")
        # resp = item['FirstName'] + ' ' + item['LastName'] + ' ' + str(item['Age'])
        return {'statusCode': 200, 'body': response['Items'], 'headers' : headers}




# def lambda_handler(event, context):

#     return {
#         "statusCode": 200,
#         'headers' : {
#             'Content-Type' : 'application/json',
#             'Access-Control-Allow-Origin': '*'
#         },
#         "body": json.dumps({
#             "message": "hello world",
#             "simplesocial" : 'simplesocial/user/get',
#             # "location": ip.text.replace("\n", "")
#         }),
#     }
