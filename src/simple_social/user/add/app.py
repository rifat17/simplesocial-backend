import os
import json
import boto3
import traceback
from botocore.exceptions import ClientError
from pydantic import ValidationError
from hashlib import sha256

# import from layers
from user import UserBody
from utils import log_in_console



def lambda_handler(event, context):

    # Get environment variables
    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    aws_environment = os.environ['AWSENV']
    host = os.environ['DOCKER_NETWORK_NAME']
    headers = {
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin': '*'
            }

    # Check if executing locally or on AWS, and configure DynamoDB connection accordingly.
    if aws_environment == "AWS_SAM_LOCAL":
        # SAM LOCAL
        # Environment is Linux
        simple_social_table = boto3.resource('dynamodb', endpoint_url=f"http://{host}:8000").Table(table_name)
    else:
        # AWS
        simple_social_table = boto3.resource('dynamodb', region_name=region).Table(table_name)

    try:

        bodydict = json.loads(event['body'])
        log_in_console('RECEIVED data', bodydict)
        user = UserBody(**bodydict)
        user.PK = user.username
        user.SK = user.username
        log_in_console('USER to be inserted', user.dict())
    except ValidationError as e:
        return {
            'statusCode': 400,
            'body': e.json(),
            'headers': headers,
        }


    # Put item in the DynamoDB table
    try:
        simple_social_table.put_item(
            Item={
                'PK': f'USER#{user.username}',
                'SK': f'USER#{user.username}', 
                'address': user.address,
                'password': sha256(user.password.encode()).hexdigest(),
                'age': user.age,
                'username' : user.username,
            },
            ConditionExpression = f'attribute_not_exists(#pk)',
            ExpressionAttributeNames={
            "#pk": "PK"
            },
            # ReturnValues='ALL_NEW' # BUG : 3 hours loss
        )
    except ClientError as e:
        # if e.response['Error']['Code'] == 'ConditionalCheckFailedException':
        #     print(e.response)
        # traceback.print_exc()
        return {'statusCode': 400, 'body': e.response['Error'], 'headers': headers,}
    else:
        print ('Item inserted')
        return {'statusCode': 200, 'body': user.json(), 'headers': headers,}  





# def lambda_handler(event, context):

#     return {
#         "statusCode": 200,
#         'headers' : {
#             'Content-Type' : 'application/json',
#             'Access-Control-Allow-Origin': '*'
#         },
#         "body": json.dumps({
#             "message": "hello world",
#             "simplesocial" : 'simplesocial/user/add',
#             # "location": ip.text.replace("\n", "")
#         }),
#     }
