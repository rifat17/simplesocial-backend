import os
import json
import boto3
from botocore.exceptions import ClientError
from user import UserBody

# from lambda_layers.resources.python.user import UserBody


def lambda_handler(event, context):

    # Get environment variables
    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    aws_environment = os.environ['AWSENV']
    host = os.environ['DOCKER_NETWORK_NAME']

    headers = {
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin': '*'
            }

    # Check if executing locally or on AWS, and configure DynamoDB connection accordingly.
    if aws_environment == "AWS_SAM_LOCAL":
        # SAM LOCAL
        # Environment is Linux
        person_table = boto3.resource('dynamodb', endpoint_url=f"http://{host}:8000").Table(table_name)
    else:
        # AWS
        person_table = boto3.resource('dynamodb', region_name=region).Table(table_name)
    try:
        print(event['body'])
        bodydict = json.loads(event['body'])
    except:
        return {
            'statusCode': 400,
            'body': 'malformed JSON',
            'headers' : headers
        }


    try:
        print(bodydict)
        response = person_table.get_item(
            
            Key={
                'PK': f'USER#{bodydict["PK"]}',
                'SK': f'USER#{bodydict["SK"]}'
            }
        )
        item = response['Item']
        print(response)
        print("GetItem succeeded:")
        user = UserBody(**item)
        user.password = ''
        return {'statusCode': 200, 'body': user.json(), 'headers' : headers}

    except ClientError as e:
        print(e.response['Error']['Message'])
        return {'statusCode': 400, 'body': e.response['Error']['Message'], 'headers': headers}
    except KeyError as e:
        print(f'Exception {e}')
        return {'statusCode': 400, 'body': 'User not found', 'headers' : headers}




# def lambda_handler(event, context):

#     return {
#         "statusCode": 200,
#         'headers' : {
#             'Content-Type' : 'application/json',
#             'Access-Control-Allow-Origin': '*'
#         },
#         "body": json.dumps({
#             "message": "hello world",
#             "simplesocial" : 'simplesocial/user/get',
#             # "location": ip.text.replace("\n", "")
#         }),
#     }
