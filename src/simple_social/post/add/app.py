import os
import json
import boto3
import traceback
from botocore.exceptions import ClientError
from pydantic import ValidationError
from ksuid import ksuid
from datetime import datetime

# import from layers
from post import AddPostReqBody
import constants
from utils import log_in_console

# from lambda_layers.resources.python.post import AddPostReqBody
# from lambda_layers.resources.python import constants



def lambda_handler(event, context):

    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    aws_environment = os.environ['AWSENV']
    host = os.environ['DOCKER_NETWORK_NAME']
    headers = {
            'Content-Type' : 'application/json',
            'Access-Control-Allow-Origin': '*'
        }


    # Check if executing locally or on AWS, and configure DynamoDB connection accordingly.
    if aws_environment == "AWS_SAM_LOCAL":
        # SAM LOCAL
        # Environment is Linux
        simple_social_table = boto3.resource('dynamodb', endpoint_url=f"http://{host}:8000").Table(table_name)
    else:
        # AWS
        simple_social_table = boto3.resource('dynamodb', region_name=region).Table(table_name)



    try:

        bodydict = json.loads(event['body'])
        log_in_console('RECEIVED data', bodydict)
        post = AddPostReqBody(**bodydict)
        posted_at = datetime.now().strftime(constants.DATETIME_FORMAT) if post.posted_at is None else post.posted_at  
        post.posted_at = posted_at

        pk = post.postOwner # f'USER#{post.postOwner}'
        sk = f'#POST#{ksuid()}'

        post.PK = pk
        post.SK = sk
        post.GSI1SK = sk
        log_in_console('POST to be inserted', post.dict())
    except ValidationError as e:
        log_in_console('ERROR putting POST', e.json())
        return {
            'statusCode': 400,
            'body': e.json(),
            'headers': headers,
        }

    try:
        simple_social_table.put_item(
            Item = post.dict()
        )
    except ClientError as e:
        log_in_console('ERROR putting POST', e)

        return {'statusCode': 400, 'body': e.response['Error'], 'headers': headers,}

    else:
        print ('Item inserted')
        return {
            'statusCode': 200,
            'body': post.json(), 
            'headers': headers
        }  




    # return {
    #     "statusCode": 200,
    #     "body": json.dumps({
    #         "message": "hello world",
    #         "simplesocial" : 'simplesocial/post/add',
    #         # "location": ip.text.replace("\n", "")
    #     }),
    # }


# {
#     "postOwner": "hasib1",
#     "postContent": "postContentpostContentpostContentpostContentpostContent",
#     "postType": "article",
#     "showMetadata": false,
#     "posted_at": "2021-07-11T14:33:49",
#     "PK": "USER#hasib1",
#     "SK": "#POST#0d78b34dbb2447b0ab6d873faeb88825c9553886",
#     "GSI1SK": "#POST#0d78b34dbb2447b0ab6d873faeb88825c9553886",
#     "GSI1PK": "ALL_POST"
# }