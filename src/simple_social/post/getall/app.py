import os
import json
import boto3
from boto3.dynamodb.conditions import Key
import traceback
from botocore.exceptions import ClientError



# import from layers
from post import Posts
from utils import log_in_console


def lambda_handler(event, context):

    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    aws_environment = os.environ['AWSENV']
    host = os.environ['DOCKER_NETWORK_NAME']
    INDEX = os.environ['GSINDEX1']
    headers = {
            'Content-Type' : 'application/json',
            'Access-Control-Allow-Origin': '*'
        }


    # Check if executing locally or on AWS, and configure DynamoDB connection accordingly.
    if aws_environment == "AWS_SAM_LOCAL":
        # SAM LOCAL
        # Environment is Linux
        simple_social_table = boto3.resource('dynamodb', endpoint_url=f"http://{host}:8000").Table(table_name)
    else:
        # AWS
        simple_social_table = boto3.resource('dynamodb', region_name=region).Table(table_name)


    try:
        response = simple_social_table.query(
            IndexName=INDEX,
            KeyConditionExpression=Key('GSI1PK').eq('ALL_POST') & Key('GSI1SK').begins_with('#POST'),
            ScanIndexForward=False,
        )
        posts = Posts(posts=response['Items'])
        log_in_console('QUERY',posts.dict())

    except ClientError as e:
        log_in_console('ERROR: ClientError: putting POST', e)

        return {'statusCode': 400, 'body': e.response['Error'], 'headers': headers,}

    else:
        print ('Item fetched')
        return {
            'statusCode': 200,
            'body': posts.json(), 
            'headers': headers
        }  








    # return {
    #     "statusCode": 200,
    #     "body": json.dumps({
    #         "message": "hello world",
    #         "simplesocial" : 'simplesocial/post/getall',
    #         # "location": ip.text.replace("\n", "")
    #     }),
    # }



# {
#     "postOwner": "Hasib",
#     "postContent": "postContentpostContentpostContentpostContentpostContent",
#     "postType": "post",
#     "showMetadata": true,
#     "posted_at": "2021-07-11T23:36:27",
#     "PK": "USER#Hasib",
#     "SK": "#POST#0d79327b5a09bc136576621420b1d64bfd8cf7df",
#     "GSI1SK": "#POST#0d79327b5a09bc136576621420b1d64bfd8cf7df",
#     "GSI1PK": "ALL_POST"
# },
# {
#     "postOwner": "Hasib",
#     "postContent": "postContentpostContentpostContentpostContentpostContent",
#     "postType": "post",
#     "showMetadata": false,
#     "posted_at": "2021-07-11T23:36:14",
#     "PK": "USER#Hasib",
#     "SK": "#POST#0d79326e0df024598a53d693df03d7909184b105",
#     "GSI1SK": "#POST#0d79326e0df024598a53d693df03d7909184b105",
#     "GSI1PK": "ALL_POST"
# },
# {
#     "postOwner": "Hasib",
#     "postContent": "postContentpostContentpostContentpostContentpostContent",
#     "postType": "article",
#     "showMetadata": false,
#     "posted_at": "2021-07-11T23:35:53",
#     "PK": "USER#Hasib",
#     "SK": "#POST#0d793259accfc1896d9e23b212b08924be8b2510",
#     "GSI1SK": "#POST#0d793259accfc1896d9e23b212b08924be8b2510",
#     "GSI1PK": "ALL_POST"
# }