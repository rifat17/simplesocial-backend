import os
import json
import boto3
from boto3.dynamodb.conditions import Key
import traceback
from botocore.exceptions import ClientError



# import from layers
from post import Posts, GetPostByPostIDReqBody
from utils import log_in_console

# local import for type hinting -_-
# comment it out before build
# from lambda_layers.resources.python.post import GetPostByPostIDReqBody, Posts

def lambda_handler(event, context):

    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    aws_environment = os.environ['AWSENV']
    host = os.environ['DOCKER_NETWORK_NAME']
    INDEX = os.environ['GSINDEX1']
    headers = {
            'Content-Type' : 'application/json',
            'Access-Control-Allow-Origin': '*'
        }


    # Check if executing locally or on AWS, and configure DynamoDB connection accordingly.
    if aws_environment == "AWS_SAM_LOCAL":
        # SAM LOCAL
        # Environment is Linux
        simple_social_table = boto3.resource('dynamodb', endpoint_url=f"http://{host}:8000").Table(table_name)
    else:
        # AWS
        simple_social_table = boto3.resource('dynamodb', region_name=region).Table(table_name)


    try:

        bodydict = json.loads(event['body'])
        postdata = GetPostByPostIDReqBody(**bodydict)
        log_in_console('GOT POST ID', postdata.dict())
    except Exception as e:
        return {
            'statusCode': 400,
            'body': e,
            'headers': headers,
        }

    try:
        response = simple_social_table.query(
            IndexName=INDEX,
            KeyConditionExpression=Key('GSI1PK').eq('ALL_POST') & Key('GSI1SK').eq(postdata.postID),
            ScanIndexForward=False,
        )
        posts = Posts(posts=response['Items'])
        log_in_console('GetPostsByPOSTID',posts.dict())

    except ClientError as e:
        log_in_console('ERROR: ClientError: getbypostid', e)

        return {'statusCode': 400, 'body': e.response['Error'], 'headers': headers,}

    else:
        print ('Item fetched')
        return {
            'statusCode': 200,
            'body': posts.json(), 
            'headers': headers
        }  








    # return {
    #     "statusCode": 200,
    #     "body": json.dumps({
    #         "message": "hello world",
    #         "simplesocial" : 'simplesocial/post/getall',
    #         # "location": ip.text.replace("\n", "")
    #     }),
    # }
