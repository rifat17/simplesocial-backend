import os
import json
import boto3
import traceback
from botocore.exceptions import ClientError
from pydantic import ValidationError
from ksuid import ksuid
from datetime import datetime

# import from layers
#uncomment it before build
from postcomment import AddCommentReqBody
import constants
from utils import log_in_console

#comment it out before build
# from lambda_layers.resources.python.postcomment import AddCommentReqBody
# from lambda_layers.resources.python import constants


def lambda_handler(event, context):

    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    aws_environment = os.environ['AWSENV']
    host = os.environ['DOCKER_NETWORK_NAME']
    headers = {
            'Content-Type' : 'application/json',
            'Access-Control-Allow-Origin': '*'
        }


    # Check if executing locally or on AWS, and configure DynamoDB connection accordingly.
    if aws_environment == "AWS_SAM_LOCAL":
        # SAM LOCAL
        # Environment is Linux
        simple_social_table = boto3.resource('dynamodb', endpoint_url=f"http://{host}:8000").Table(table_name)
    else:
        # AWS
        simple_social_table = boto3.resource('dynamodb', region_name=region).Table(table_name)

# Comment,
# commentedby=USER#<USERNAME>, 
# postID=POST#<KSUID>, 
# commented_at
# PK=POST#<KSUID>,
# SK=#COMMENT#<KSUID>,
# GSI1PK=POST#<KSUID>,
# GSI1SK=#COMMENT#<KSUID>,

    try:

        bodydict = json.loads(event['body'])
        log_in_console('RECEIVED data', bodydict)
        commentData = AddCommentReqBody(**bodydict)
        # commentData.commentedAt = datetime.now().strftime(constants.DATETIME_FORMAT)
        # received postID=#POST#0d79e82753254bd98de0eb50f6a378126e6af058
        # make it postID=POST#0d79e82753254bd98de0eb50f6a378126e6af058
        commentData.postID = commentData.postID[1:] if commentData.postID[0] == '#' else commentData.postID

        pk = f'COMMENT#{ksuid()}'
        sk = pk

        commentData.PK = pk
        commentData.SK = sk
        commentData.GSI1PK = commentData.postID
        commentData.GSI1SK = f'#{pk}'
        log_in_console('COMMENT to be inserted', commentData.dict())
    except ValidationError as e:
        log_in_console('ERROR putting COMMENT', e.json())
        return {
            'statusCode': 400,
            'body': e.json(),
            'headers': headers,
        }

    try:
        print("REQUESTING TO DYNAMO")
        simple_social_table.put_item(
            Item = commentData.dict()
        )
    except ClientError as e:
        log_in_console('ERROR putting COMMENT', e)

        return {'statusCode': 400, 'body': e.response['Error'], 'headers': headers,}

    else:
        print ('Item inserted')
        return {
            'statusCode': 200,
            'body': commentData.json(), 
            'headers': headers
        }  
