import os
import json
import boto3
from boto3.dynamodb.conditions import Key
import traceback
from botocore.exceptions import ClientError



# import from layers
from postcomment import Comments, GetCommentsByPostIDReqBody
from utils import log_in_console

# local import for type hinting -_-
# comment it out before build
# from lambda_layers.resources.python.postcomment import Comments, GetCommentsByPostIDReqBody

def lambda_handler(event, context):

    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    aws_environment = os.environ['AWSENV']
    host = os.environ['DOCKER_NETWORK_NAME']
    INDEX = os.environ['GSINDEX1']
    headers = {
            'Content-Type' : 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
            'Access-Control-Allow-Headers': 'Content-Type'
        
    }

    log_in_console("EVENT", event)


    # Check if executing locally or on AWS, and configure DynamoDB connection accordingly.
    if aws_environment == "AWS_SAM_LOCAL":
        # SAM LOCAL
        # Environment is Linux
        simple_social_table = boto3.resource('dynamodb', endpoint_url=f"http://{host}:8000").Table(table_name)
    else:
        # AWS
        simple_social_table = boto3.resource('dynamodb', region_name=region).Table(table_name)


    try:
        log_in_console("EVENT", event['queryStringParameters'])

        bodydict = event['queryStringParameters']
        log_in_console('BODY DICT ', bodydict)
        commentData = GetCommentsByPostIDReqBody(**bodydict)
        commentData.postID = commentData.postID[1:] if commentData.postID[0] == '#' else commentData.postID
        log_in_console('GOT POST ID', commentData.dict())
    except Exception as e:
        print(e)
        return {
            'statusCode': 400,
            'body': e,
            'headers': headers,
        }

    try:
        response = simple_social_table.query(
            IndexName=INDEX,
            KeyConditionExpression=Key('GSI1PK').eq(commentData.postID) & Key('GSI1SK').begins_with('#COMMENT'),
            ScanIndexForward=False,
        )
        comments = Comments(comments=response['Items'])
        log_in_console('GetCommentsByPOSTID',comments.dict())

    except ClientError as e:
        log_in_console('ERROR: ClientError: GetCommentsByPOSTID', e)

        return {'statusCode': 400, 'body': e.response['Error'], 'headers': headers,}

    else:
        print ('Item fetched')
        return {
            'statusCode': 200,
            'body': comments.json(), 
            'headers': headers
        }  








    # return {
    #     "statusCode": 200,
    #     "body": json.dumps({
    #         "message": "hello world",
    #         "simplesocial" : 'simplesocial/post/getall',
    #         # "location": ip.text.replace("\n", "")
    #     }),
    # }
